# Test Exercise

## Prerequisites

* JDK 8
* Maven 3
* either PostgreSQL or Docker and Docker Container

## Installation

* Build by running:
```
mvn clean install
unzip -d ${INSTALL_DIR} container/target/testex-container-*-dist.zip
```
where `${INSTALL_DIR}` corresponds to desired installation directory

* Either install PostgreSQL and create user and database for this exercise:
```
create database testex;
create user testex with password 'testex';
grant all on database testex to testex;
```

* or launch a PostgreSQL as a Docker container:
```
docker-compose up -d
```

* Configure database enpoint in `${INSTALL_DIR}/testex/testex.properties`:
```
spring.datasource.url=jdbc:postgresql://172.17.0.2:5432/testex?user=testex&password=testex
```

NB! docker container ip address can be obtained using:
```
docker inspect `docker ps -a | grep testex_sql_1 | cut -f 1 -d ' '` | grep IPAddress
```

* In order to start, run in `${INSTALL_DIR}/testex` directory:
```
./run.sh start
```

* and in order to stop:
```
./run.sh stop
```

* Access from: <http://localhost:3030>

## Running for developers inside IDE-s

* Configure database endpoint in `app/src/main/resources/application.properties`
* Start from `main` method in `app/src/main/java/eu/ojandu/testex/TestEx`
* Access from: <http://localhost:8080>



