#!/bin/bash

source ./env.sh

if [[ $(id -u) -ne $(id -u "$ALLOWED_USER") ]]; then
    echo "Log in as user $ALLOWED_USER and try again"
    exit 0
fi

start() {
    if [[ $PROTSESS -eq 0 ]]; then
        CPATH=.;

        for i in `find $APP_HOME/lib -name *.jar`; do
            if test -f $i; then
                CPATH=${CPATH}\:$i;
            fi
        done

        echo $CPATH;

        nohup $JAVA \
          -Djava.security.policy=security.policy \
          ${PROTSESS_GREP} \
          -Dcom.sun.management.jmxremote \
          -Dcom.sun.management.jmxremote.port=$JMX_PORT \
          -Dcom.sun.management.jmxremote.ssl=false \
          -Dcom.sun.management.jmxremote.authenticate=false \
          -Djava.net.preferIPv4Stack=true \
          -Xmx1024m \
          -classpath ${CPATH} eu.ojandu.testex.TestEx  $* \
          --spring.config.location=classpath:/default.properties,classpath:/testex.properties \
          2>> $ERRLOG \
          1>> $STDLOG &

        PROTSESS=$(echo $!);
        echo "Started $APP_NAME with ID=$PROTSESS"
    else
        echo "$APP_NAME is already running ID=$PROTSESS"
    fi
}

stop() {
    if [[ $PROTSESS -gt 0 ]]; then
        echo "Stopping $APP_NAME"
        if [ "$1" == "force" ]; then
            echo "Killing $PROTSESS by force"
            kill -9 $PROTSESS
        else
            echo "Killing $PROTSESS"
            kill $PROTSESS > /dev/null 2>&1
        fi
        while [[ $PROTSESS -ne 0 ]]; do
            PROTSESS=$[$(pgrep -f "$PROTSESS_GREP")+0]
            sleep 3
            echo "stopping..."
        done
    else
        echo "$APP_NAME is not running"
    fi
}

case "$1" in
    start)
        start
        ;;
    stop)
        if [[ "$2" == "force" ]]; then
            stop force
        else
            stop
        fi
        ;;
    restart)
        stop
        start
        ;;
    id)
        echo $PROTSESS
        ;;
    *)
        if [[ $PROTSESS -gt 0 ]]; then
            echo "$APP_NAME is running ID=$PROTSESS"
        else
            echo "$APP_NAME is not running"
        fi
        echo "Usage: $0 (start|stop|stop force|restart)"
esac