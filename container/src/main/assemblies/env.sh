#!/bin/bash

JAVA_HOME=""
JAVA="java"
PROTSESS_GREP=" -Dinstance.name=testex "
PROTSESS=$[`pgrep -f "$PROTSESS_GREP"`+0]
APP_NAME="Testex"
APP_HOME=$(pwd)
STDLOG="$APP_HOME/logs/std.out"
ERRLOG="$APP_HOME/logs/std.err"
ALLOWED_USER=$(id -u -n)
JMX_PORT=8555