package eu.ojandu.testex.ctrl;

import eu.ojandu.testex.entity.WebUser;
import eu.ojandu.testex.exception.UserAlreadyExistsException;
import eu.ojandu.testex.model.NewUserModel;
import eu.ojandu.testex.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.validation.Valid;
import java.util.Arrays;

@Controller
public class SessionCtrl {

    private static final String NEW_USER = "newUser";

    @Autowired
    private UserService userService;

    @RequestMapping(path = "/public/login", method = RequestMethod.GET)
    public String loginView(
            @RequestParam(value = "error", required = false) String failed,
            Model model
    ) {
        if(failed != null) {
            model.addAttribute("error", true);
        } else {
            model.addAttribute("error", false);
        }

        return "public/login";
    }

    @RequestMapping(path = "/public/new-user", method = RequestMethod.GET)
    public String newUserView(
            Model model
    ) {
        model.addAttribute("newUser", new NewUserModel());
        return "public/new-user";
    }

    @RequestMapping(path = "/public/new-user", method = RequestMethod.POST)
    public String newUser(
            @Valid @ModelAttribute(NEW_USER) NewUserModel newUserModel,
            BindingResult bindingResult,
            Model model
    ) {
        model.addAttribute(NEW_USER, newUserModel);
        if (bindingResult.hasErrors()) {
            return "public/new-user";
        }

        if(!newUserModel.getPassword1().equals(newUserModel.getPassword2())) {
            bindingResult.addError(new FieldError(NEW_USER, "password2", "passwords must match"));
            return "public/new-user";
        }
        try {
            WebUser webUser = userService.newUser(newUserModel.getUsername(), newUserModel.getPassword1());
            SecurityContextHolder.getContext().setAuthentication(new UsernamePasswordAuthenticationToken(
                    webUser.getUsername(),
                    webUser.getUsername(),
                    Arrays.asList(new SimpleGrantedAuthority("ROLE_USER"))
            ));
        } catch (UserAlreadyExistsException e) {
            bindingResult.addError(new FieldError(NEW_USER, "username", "already used"));
            return "public/new-user";
        }

        return "redirect:/";
    }
}
