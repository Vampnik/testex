package eu.ojandu.testex.ctrl;

import eu.ojandu.testex.entity.Sector;
import eu.ojandu.testex.entity.WebUser;
import eu.ojandu.testex.model.FormModel;
import eu.ojandu.testex.repo.SectorRepo;
import eu.ojandu.testex.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.validation.Valid;
import java.util.Comparator;
import java.util.stream.Collectors;

@Controller
public class AppCtrl {

    private static final String SECTORS = "sectorsModel";

    @Autowired
    private SectorRepo sectorRepo;

    @Autowired
    private UserService userService;

    @RequestMapping(path = "/", method = RequestMethod.GET)
    public String index(ModelMap model) {

        WebUser webUser = userService.getSessionUser();
        FormModel formModel = new FormModel();
        formModel.setName(webUser.getName());
        formModel.setSectors(webUser.getSectors().stream().map(s -> s.getValue()).collect(Collectors.toSet()));
        model.addAttribute(SECTORS, formModel);

        model.addAttribute("allSectors", sectorRepo.findAll().stream()
                .sorted(Comparator.comparing(Sector::getOrderNum))
                .collect(Collectors.toList()));

        return "index";
    }

    @RequestMapping(path = "/", method = RequestMethod.POST)
    public String formPost(
            @Valid @ModelAttribute(SECTORS) FormModel formModel,
            BindingResult bindingResult,
            Model model
    ) {
        if(!bindingResult.hasErrors()) {
            if(formModel.isTerms()) {
                WebUser webUser = userService.getSessionUser();
                webUser.setSectors(sectorRepo.findAllByValue(formModel.getSectors()).stream()
                        .collect(Collectors.toSet()));
                webUser.setName(formModel.getName());
                webUser.setAgreedToTermsOn(System.currentTimeMillis());
                userService.save(webUser);
            } else {
                bindingResult.addError(new FieldError(SECTORS, "terms", "Agreement to terms required"));
            }
        }

        model.addAttribute("allSectors", sectorRepo.findAll().stream()
                .sorted(Comparator.comparing(Sector::getOrderNum))
                .collect(Collectors.toList()));

        return "index";
    }
}
