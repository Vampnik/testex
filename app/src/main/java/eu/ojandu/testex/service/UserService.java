package eu.ojandu.testex.service;

import eu.ojandu.testex.entity.WebUser;
import eu.ojandu.testex.exception.UserAlreadyExistsException;
import eu.ojandu.testex.repo.WebUserRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class UserService {

    @Autowired
    private WebUserRepo webUserRepo;

    @Autowired
    private PasswordEncoder passwordEncoder;

    public WebUser newUser(String username, String password) throws UserAlreadyExistsException {
        WebUser webUser = webUserRepo.findByUsername(username);
        if(webUser != null) {
            throw new UserAlreadyExistsException();
        }

        webUser = new WebUser();

        webUser.setUsername(username);
        webUser.setHash(passwordEncoder.encode(password));
        webUser.setCreatedOn(System.currentTimeMillis());

        webUser = webUserRepo.save(webUser);

        return webUser;
    }

    public WebUser save(WebUser webUser) {
        return webUserRepo.save(webUser);
    }

    public WebUser getSessionUser() {
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if(principal instanceof WebUser) {
            return webUserRepo.findByUsername(((WebUser) principal).getUsername());
        } else if(principal instanceof String) {
            return webUserRepo.findByUsername((String) principal);
        } else {
            throw new IllegalArgumentException("Unknown principal class " + principal.getClass());
        }
    }

}
