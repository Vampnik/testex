package eu.ojandu.testex.repo;

import eu.ojandu.testex.entity.WebUser;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface WebUserRepo extends JpaRepository<WebUser, Long> {

    WebUser findByUsername(String username);

}
