package eu.ojandu.testex.repo;

import eu.ojandu.testex.entity.Sector;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SectorRepo extends JpaRepository<Sector, Long> {

    @Query("from Sector where value in ?1")
    List<Sector> findAllByValue(Iterable<String> iterable);

}
