package eu.ojandu.testex;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableAutoConfiguration
@ComponentScan("eu.ojandu.testex")
public class TestEx {

    public static void main(String[] args) {
        SpringApplication.run(TestEx.class, args);
    }

}
