package eu.ojandu.testex.model;

import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;

public class NewUserModel {

    @NotNull
    @Length(min = 3, max = 50)
    private String username;

    @NotNull
    @Length(min = 3, max = 50)
    private String password1;

    @NotNull
    @Length(min = 3, max = 50)
    private String password2;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword1() {
        return password1;
    }

    public void setPassword1(String password1) {
        this.password1 = password1;
    }

    public String getPassword2() {
        return password2;
    }

    public void setPassword2(String password2) {
        this.password2 = password2;
    }
}
