package eu.ojandu.testex.model;

import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Set;

public class FormModel {

    @NotBlank
    private String name;

    @NotNull
    @Size(min = 1, max = 1000)
    private Set<String> sectors;

    private boolean terms;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<String> getSectors() {
        return sectors;
    }

    public void setSectors(Set<String> sectors) {
        this.sectors = sectors;
    }

    public boolean isTerms() {
        return terms;
    }

    public void setTerms(boolean terms) {
        this.terms = terms;
    }

    @Override
    public String toString() {
        return "FormModel{" +
                "name='" + name + '\'' +
                ", sectors=" + sectors +
                '}';
    }
}
